import express from 'express';

const app = express();

app.get('/', (req, res) => {
  const val = {
    message: 'hello',
    'current time': new Date(),
  };
  res.send(val);
});
app.listen(8080, () => {
  console.log('server started!');
});
