FROM node:12.13-alpine
WORKDIR /usr/src/app
COPY ["package*.json", "./"]
RUN npm install --silent
COPY . .
ENV NODE_ENV production
RUN npm run tsc
EXPOSE 8080
CMD npm start